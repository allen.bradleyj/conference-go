from django.http import JsonResponse
from common.json import ModelEncoder
from .models import Presentation
from events.models import Conference
from events.api_views import ConferenceListEncoder
from .models import Status
from django.views.decorators.http import require_http_methods
import json
from events.acls import get_geocode, get_weather
import pika


def send_to_queue(input, status):
    sending_dict = {
        "presenter_name": input.presenter_name,
        "presenter_email": input.presenter_email,
        "title": input.title,
    }
    message = json.dumps(sending_dict)
    parameters = pika.ConnectionParameters(host="rabbitmq")
    connection = pika.BlockingConnection(parameters)
    channel = connection.channel()
    channel.queue_declare(queue=status)
    channel.basic_publish(
        exchange="",
        routing_key=status,
        body=message,
    )
    connection.close()


@require_http_methods(["PUT"])
def api_approve_presentation(request, pk):
    presentation = Presentation.objects.get(id=pk)
    presentation.approve()
    status = "presentation_approvals"
    send_to_queue(presentation, status)
    return JsonResponse(
        presentation,
        encoder=PresentationDetailEncoder,
        safe=False,
    )


@require_http_methods(["PUT"])
def api_reject_presentation(request, pk):
    presentation = Presentation.objects.get(id=pk)
    presentation.reject()
    status = "presentation_rejections"
    send_to_queue(presentation, status)
    return JsonResponse(
        presentation,
        encoder=PresentationDetailEncoder,
        safe=False,
    )


class StatusListEncoder(ModelEncoder):
    model = Status
    properties = ["name"]


class PresentationListEncoder(ModelEncoder):
    model = Presentation
    properties = [
        "title",
        "status",
    ]

    encoders = {"status": StatusListEncoder()}


@require_http_methods(["GET", "POST"])
def api_list_presentations(request, conference_id):
    """
    Lists the presentation titles and the link to the
    presentation for the specified conference id.

    Returns a dictionary with a single key "presentations"
    which is a list of presentation titles and URLS. Each
    entry in the list is a dictionary that contains the
    title of the presentation, the name of its status, and
    the link to the presentation's information.

    {
        "presentations": [
            {
                "title": presentation's title,
                "status": presentation's status name
                "href": URL to the presentation,
            },
            ...
        ]
    }
    """
    if request.method == "GET":
        presenter = Presentation.objects.all()
        return JsonResponse(
            presenter, encoder=PresentationListEncoder, safe=False
        )
    else:

        content = json.loads(request.body)

        try:
            conference = Conference.objects.get(id=content["conference"])
            content["conference"] = conference

        except Conference.DoesNotExist:
            return JsonResponse(
                {"message": "Invalid conference id"}, status=400
            )

        presenter = Presentation.create(**content)
        return JsonResponse(
            presenter, encoder=PresentationDetailEncoder, safe=False
        )


class PresentationDetailEncoder(ModelEncoder):
    model = Presentation
    properties = [
        "presenter_name",
        "company_name",
        "presenter_email",
        "title",
        "synopsis",
        "created",
        "status",
        "conference",
    ]
    encoders = {
        "status": StatusListEncoder(),
        "conference": ConferenceListEncoder(),
    }


@require_http_methods(["GET", "DELETE", "PUT"])
def api_show_presentation(request, id):
    """
    Returns the details for the Presentation model specified
    by the id parameter.

    This should return a dictionary with the presenter's name,
    their company name, the presenter's email, the title of
    the presentation, the synopsis of the presentation, when
    the presentation record was created, its status name, and
    a dictionary that has the conference name and its URL

    {
        "presenter_name": the name of the presenter,
        "company_name": the name of the presenter's company,
        "presenter_email": the email address of the presenter,
        "title": the title of the presentation,
        "synopsis": the synopsis for the presentation,
        "created": the date/time when the record was created,
        "status": the name of the status for the presentation,
        "conference": {
            "name": the name of the conference,
            "href": the URL to the conference,
        }
    }
    """

    if request.method == "GET":
        presenter = Presentation.objects.get(id=id)
        return JsonResponse(
            presenter, encoder=PresentationDetailEncoder, safe=False
        )

    elif request.method == "DELETE":
        count, _ = Presentation.objects.filter(id=id).delete()
        return JsonResponse({"deleted": count > 0})
    else:
        content = json.loads(request.body)
        presenter = Presentation.objects.get(id=id)
        conference_id = content["conference"]
    try:
        conference = Conference.objects.get(id=conference_id)
        # if conference in content:
        # conference = Conference.objects.get(id=content["conference"])
        # content["conference"] = conference
    except Conference.DoesNotExist:
        return JsonResponse({"message": "Invalid conference id"})

    # except

    Presentation.objects.filter(id=id).update(**content)

    return JsonResponse(
        presenter,
        encoder=PresentationDetailEncoder,
        safe=False,
    )
