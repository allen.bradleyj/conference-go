from .keys import PEXELS_API_KEY, GEOCODE_API_KEY
import requests
import math


def get_photo(city, state):
    url = f"https://api.pexels.com/v1/search?query={city},+{state}"
    header = {"Authorization": PEXELS_API_KEY}

    response = requests.get(url, headers=header)
    data = response.json()
    photo = {"original_url": data["photos"][0]["src"]["original"]}
    photo_url = photo["original_url"]
    return photo_url


def get_geocode(city_name, state_code):
    url = f"http://api.openweathermap.org/geo/1.0/direct?q={city_name},{state_code},US&limit=1&appid={GEOCODE_API_KEY}"

    response = requests.get(url)
    data = response.json()
    coords = {"lat": data[0]["lat"], "lon": data[0]["lon"]}
    return coords


def get_weather(coords):
    url = f"https://api.openweathermap.org/data/2.5/weather"
    coords["appid"] = GEOCODE_API_KEY

    response = requests.get(url, params=coords)
    data = response.json()
    weather_data = {
        "weather": {
            "Current Temp": math.ceil(
                ((data["main"]["temp"]) - 273.15) * 9 / 5 + 32
            ),
            "description": data["weather"][0]["description"],
        }
    }

    return weather_data
