from json import JSONEncoder
from datetime import datetime
from django.db.models import QuerySet

class QuerySetEncoder(JSONEncoder):
    def default(self, object):
        if isinstance(object, QuerySet):
            return list(object)
        else:
            return super().default(object)


class DateEncoder(JSONEncoder):
    def default(self, object):
        if isinstance(object, datetime):
            return object.isoformat()
        # if o is an instance of datetime
        #    return o.isoformat()
        else:
            return super().default(object)
        # otherwise
        #    return super().default(o)


class ModelEncoder(DateEncoder, QuerySetEncoder, JSONEncoder):
    encoders = {}
    def default(self, object):
        #   if the object to decode is the same class as what's in the
        if isinstance(object, self.model):
            dict = {}
            if hasattr(object, "get_api_url"):
                dict["href"] = object.get_api_url()
            #   model property, then
            #     * create an empty dictionary that will hold the property names
            #       as keys and the property values as values

            #     * for each name in the properties list
            #         * get the value of that property from the model instance
            #           given just the property name
            #         * put it into the dictionary with that property name as
            #           the key
            for property in self.properties:
                value = getattr(object, property)
                if property in self.encoders:
                    encoder = self.encoders[property]
                    value = encoder.default(value)
                dict[property] = value
            dict.update(self.get_extra_data(object))
            #     * return the dictionary
            return dict
        else:
            return super().default(object)
        #   otherwise,
        #       return super().default(o)  # From the documentation

    def get_extra_data(self, object):
        return{}
